﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ThumbnailManager:MonoBehaviour
{

	public Transform container;
	public GameObject prefab;

	public 	Camera camera;

	private List<ThumbnailVO> _thumbnailVOList = new List<ThumbnailVO> ();

	void Start ()
	{
		createThumbnailVOList ();
		createThumbnailPrefabs ();
	}

	private void createThumbnailVOList ()
	{
		ThumbnailVO thumbnailVO;
		for (int i = 0; i < 1000; i++) {
			thumbnailVO = new ThumbnailVO ();
			thumbnailVO.id = i.ToString ();
			_thumbnailVOList.Add (thumbnailVO);
		}
	}

	private void createThumbnailPrefabs ()
	{
		GameObject gameObj;
		for (int i = 0; i < _thumbnailVOList.Count; i++) {
			gameObj = (GameObject)Instantiate (prefab);
			gameObj.transform.SetParent (container, false);
			gameObj.GetComponent<Thumbnail> ().thumbnailVO = _thumbnailVOList [i];
		}
	}


	/* http://www.tasharen.com/forum/index.php?PHPSESSID=a9963b42289b169af0b97a70f3e6096e&topic=7348.msg34657#msg34657
	 * According to what suggested in the readme file and the linked above, I try to implement the plan C which active and deactive items according to its position 
     * 
     * There are 2 ways I can come out to determine if the items is within the canvas.
     * 1. using the camera.worldToViewportPoint ()
     * 2. using the rect transform component of container
     * 
     * The implementation of these two approaches are below. 
     * It does activate and deactivate items accordingly but the scrollview does not update items in the screen. I don't know why. -.-|||
     * 
     * Finally I decide to try the plan A which suggested only instanstiate a limited number of items and reposition them when scrolling,
     * but it took a lot of time for me to do experiment. I didn't get this approach working.
     * 
     * Overall, I guess the main reason is that too many gameobject is created in the beginning of the game even most of them are not necesssay.
     * It eats up a lot of memory, I did try to either set the unnecessary ones off or not-rendered. But the best way is too create a small amount of items.
     * I would like to try it out if I have more time. 
     */


	//	1.
	void Update ()
	{
		for (int i = 0; i < 1000; i++) {
			Vector3 viewPos = camera.WorldToViewportPoint (container.GetChild (i).position);
	
			// seen by the camera
			if ((viewPos.x >= 0 && viewPos.x <= 1) && (viewPos.y >= 0 && viewPos.y <= 1)) {
				container.GetChild (i).gameObject.SetActive (true);
			} else {
				container.GetChild (i).gameObject.SetActive (false);
			}
		}
	}

	//  2.
	//	void Update ()
	//	{
	//		/*
	//		 * Instead of considering all screen resolution, I only work on FWVGA 480 * 854.
	//		 * It displays 9 rows in a screen size.
	//		 * If we find the starting row is is i then it displays till i+9-1 row.
	//		 * Then enable the items from i*5 - (i+9-1)*5 and disable the rest items
	//		 */
	//
	//		int visibleRowId = FindVisibleRowId ();
	//
	//		for (int i = 0; i < 1000; i++) {
	//			if (i >= visibleRowId * 5 && i < (visibleRowId + 9) * 5) {
	//				//	container.GetChild(i).gameObject.SetActive (true);
	//				container.GetChild (i).gameObject.SetActive (true);
	//			} else {
	//				container.GetChild (i).gameObject.SetActive (false);
	//			}
	//		}
	//	}
	//
	//	int FindVisibleRowId ()
	//	{
	//		// cell size = 172 && spacin = 44
	//		// container (content) shifts vertically 172 + 44/2 = 194 = A row of thumbnail
	//		for (int i = 0; i < 200; i++) {
	//			if (container.gameObject.GetComponent<RectTransform> ().anchoredPosition.y < i * 194) {
	//				if (i == 0) {
	//					Debug.Log (i);
	//					return i;
	//				} else {
	//					Debug.Log (i - 1);
	//					return i - 1;
	//				}
	//				break;
	//			}
	//		}
	//		return -100;
	//	}
}
